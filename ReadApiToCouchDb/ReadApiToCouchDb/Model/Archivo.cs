﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadApiToCouchDb.Model
{
    public class Archivo
    {
        public string type { get; set; }
        public string FileName { get; set; }
        public string api { get; set; }
        public dynamic Passanger { get; set; }
        public dynamic Flight { get; set; }


    }
}
