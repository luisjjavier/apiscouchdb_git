﻿using ReadApiToCouchDb.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Xml;


namespace ReadApiToCouchDb
{
    class Program
    {
        private static string folderDeArchivos = @"E:\Downloads\wwr";
        private static string baseURL = "http://127.0.0.1:5984/";
        static void Main(string[] args)
        {
            HttpRESTSimpleClient httpclient = new HttpRESTSimpleClient(baseURL);
            RestResponse response = new RestResponse();


            List<string> files = Directory.GetFiles(folderDeArchivos).ToList();
            foreach (var i in files)
            {
                try
                {
                    using (StreamReader sr = new StreamReader(i, Encoding.ASCII))
                    {
                       string archivoApis = sr.ReadToEnd();
                       XmlDocument documento = new XmlDocument();
                       documento.LoadXml(archivoApis);
                        dynamic todo = JsonConvert.DeserializeObject<dynamic>( JsonConvert.SerializeXmlNode(documento));
                        Model.Archivo file = new Model.Archivo();
                        file.type = "api";
                        file.api = archivoApis;
                        file.FileName = i;
                        file.Flight = todo.APISPaxlst.FlightIdentification;
                        file.Passanger = todo.APISPaxlst.Passenger;
                       httpclient.Invoke<Model.Archivo>(file,HttpMethods.POST, string.Format("/prueba"), response.Initialize);
                       if (response.Response.StatusCode== System.Net.HttpStatusCode.Created)
                       {
                           Console.WriteLine("Se a guardando correctamente {0}", i);
                       }
                    }
                   

                }
                catch (Exception)
                {
                    
                    Console.WriteLine("No se pudo leer el archivo");
                }
            }
        }
    }
}
