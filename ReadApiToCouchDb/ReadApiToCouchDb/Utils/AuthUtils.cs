﻿using ReadApiToCouchDb.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ReadApiToCouchDb.Utils
{
    public static class AuthUtils
    {
        public static AuthInfo ParseBasicAuth(string authString)
        {
            if (authString == null)
            {
                return new AuthInfo(null, null, null);
            }
            else
            {
                int index = authString.LastIndexOf(':');
                String userAndDomain = authString.Substring(0, index);
                String password = authString.Substring(index + 1);

                index = userAndDomain.LastIndexOf('@');
                String user = userAndDomain.Substring(0, index);
                String domain = userAndDomain.Substring(index + 1);

                return new AuthInfo(user, domain, password);
            }
        }

        public static string DecodeBasicAuthenticationHeader(string httpBasicAuthHeader)
        {
            if (httpBasicAuthHeader == null)
            {
                return null;
            }
            else
            {
                String authHeader = httpBasicAuthHeader.Replace("Basic ", "");
                String decodedHeader = Encoding.UTF8.GetString(Convert.FromBase64String(authHeader));

                return decodedHeader;
            }
        }

        public static String ToBase64(this String str, Encoding encoding = null)
        {
            return Convert.ToBase64String((encoding ?? Encoding.UTF8).GetBytes(str));
        }

        public static string Encript(this string str, string salt)
        {
            str = string.Concat(str, salt);
            byte[] arrbyte = new byte[str.Length];
            SHA256 hash = new SHA256CryptoServiceProvider();
            arrbyte = hash.ComputeHash(Encoding.UTF8.GetBytes(str));
            return Convert.ToBase64String(arrbyte);   
        }
    }
}
