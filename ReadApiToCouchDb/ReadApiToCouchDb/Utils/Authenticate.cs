﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadApiToCouchDb.Utils
{
    public class AuthInfo
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Domain { get; set; }

        public AuthInfo(string user, string domain, string password)
        {
            this.Username = user;
            this.Domain = domain;
            this.Password = password;
        }

    }
}
