﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace  ReadApiToCouchDb.Utils
{


    public sealed class HttpRESTSimpleClient : IDisposable
    {
        
        public string BaseUri { get; private set; }
        public string UserName { get; private set; }
        public string Password { get; private set; }

        public HttpRESTSimpleClient(string baseUri) : this(baseUri, null, null) {
        }

        public HttpRESTSimpleClient(string baseUri, string userName, string password)
        {
            this.BaseUri = baseUri;
            this.UserName = userName;
            this.Password = password;
        }

        #region No Explicit Response (with and without input)

        public HttpWebResponse Invoke(HttpMethods method, String resource = "", 
            Action<WebRequest, HttpWebResponse, String> action = null)
        {
            return Invoke<String>(null, method, resource, action);
        }

        /// <summary>
        /// make http invoke and return input Type, if an error happen return null and initilize the Error property
        /// </summary>
        /// <typeparam name="TInput"></typeparam>
        /// <param name="input"></param>
        /// <param name="method"></param>
        /// <param name="resource"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public HttpWebResponse Invoke<TInput>(TInput input, HttpMethods method, String resource = "",
            Action<WebRequest, HttpWebResponse, String> action = null)
        {
            WebRequest request = WebRequest.Create(BaseUri + resource);

            request.Method = method.ToString();

            request.ContentType = "application/json";
            if (!String.IsNullOrWhiteSpace(UserName) && !String.IsNullOrWhiteSpace(Password))
            {
                request.Headers[HttpRequestHeader.Authorization] = String.Format("{0}:{1}", UserName, Password).ToBase64();
            }
            else
            {
                request.UseDefaultCredentials = true;
                
            }

            ManageInput<TInput>(input, request);

            var response = ManageResponse(request);

            //action(request, response, null);
            string body = "";
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                body = reader.ReadToEnd();
                response.Close();
            }

            ManageAction(action, request, response, body);

            return response;
        }

        /// <summary>
        /// make http invoke and return OutPut Type, if an error happen return null and initilize the Error property
        /// </summary>
        /// <typeparam name="TInput"></typeparam>
        /// <typeparam name="TOuput"></typeparam>
        /// <param name="input"></param>
        /// <param name="method"></param>
        /// <param name="converter"></param>
        /// <param name="resource"></param>
        /// <param name="action"></param>
        /// <returns></returns>        
        public TOuput Invoke<TInput, TOuput>(TInput input, HttpMethods method, Func<String, TOuput> converter, 
            String resource = "", Action<WebRequest, HttpWebResponse, String> action = null){

                WebRequest request = WebRequest.Create(BaseUri + resource);

                request.Method = method.ToString();
                request.ContentType = "application/json";
                if (!String.IsNullOrWhiteSpace(UserName) && !String.IsNullOrWhiteSpace(Password))
                {
                    request.Headers[HttpRequestHeader.Authorization] = String.Format("{0}:{1}", UserName, Password).ToBase64();
                }
                else
                {
                    request.UseDefaultCredentials = true;
                }

                ManageInput<TInput>(input, request);

                var response = ManageResponse(request);

                if (action != null)
                {
                    action(request, response, null);
                }

                return ManageOutput<TOuput>(response);
         }

        #endregion


        #region Explicit Responses (with and without input)

        public TOutput Invoke<TOutput>(HttpMethods method, String resource = "", Action<WebRequest, HttpWebResponse, String> action = null)
        {
            return Invoke<String, TOutput>(null, method, resource, action);
        }

        public TOutput Invoke<TInput, TOutput>(TInput input, HttpMethods method, String resource = "", Action<WebRequest, HttpWebResponse, String> action = null)
        {
            WebRequest request = WebRequest.Create(BaseUri + resource);
            request.ContentType = "application/json";
            request.Method = method.ToString();
            if (!String.IsNullOrWhiteSpace(UserName) && !String.IsNullOrWhiteSpace(Password))
            {
                request.Headers[HttpRequestHeader.Authorization] = String.Format("Basic {0}", String.Format("{0}:{1}", UserName, Password).ToBase64());
            }
            else
            {
                request.UseDefaultCredentials = true;
            }

            ManageInput<TInput>(input, request);

            var response = ManageResponse(request);
            string body = "";
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                body = reader.ReadToEnd();
                response.Close();
            }
            ManageAction(action, request, response, body);
            int code = (int)response.StatusCode;
            if (code < 200 || code > 206)
            {              
               return default(TOutput);
            }
         
            TOutput output = body.FromJSON<TOutput>();

            return output;
        }

        #endregion


        #region Helpers

        private static HttpWebResponse ManageResponse(WebRequest request)
        {
            HttpWebResponse response = null;
            try
            {
                response = (HttpWebResponse)request.GetResponse();               
            }
            catch (WebException webEx)
            {
                response = (HttpWebResponse)webEx.Response;
            }

            return response;
        }

        private static void ManageInput<TInput>(TInput input, WebRequest request)
        {
            if (input != null)
            {
                Stream requestStream = request.GetRequestStream();

                String json = input.ToJSON();
                using (StreamWriter writer = new StreamWriter(requestStream))
                {
                    writer.Write(json);
                    writer.Flush();
                }
            }
        }

        private static TOutput ManageOutput<TOutput>(WebResponse response)
        {

            Stream responseStream = response.GetResponseStream();

            String json = string.Empty;
            using (StreamWriter writer = new StreamWriter(responseStream))
            {
                writer.Write(json);
                writer.Flush();
            }

            return json.FromJSON<TOutput>();
        }

        private static void ManageAction(Action<WebRequest, HttpWebResponse, String> action, WebRequest request,
            HttpWebResponse response, string body)
        {
            if (action != null)
            {
                action(request, response, body);
            }
        }
        
        #endregion

        public void Dispose()
        {
            this.BaseUri = null;
            this.UserName = null;
            this.Password = null;           
        }
    }

    public enum HttpMethods {GET, POST, PUT, DELETE}
}
