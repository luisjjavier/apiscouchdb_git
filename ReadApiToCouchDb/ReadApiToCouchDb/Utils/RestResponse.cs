﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace  ReadApiToCouchDb.Utils
{
    public class ResponseError
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }

    public class RestResponse : IDisposable
    {

        public WebRequest Request { get; set; }
        public HttpWebResponse Response { get; set; }
        public string Body { get; set; }
        public ResponseError Error { get; private set; }

        public void Initialize(WebRequest request, HttpWebResponse response, string body)
        {
          
            this.Request = request;

            response.ContentType.Insert(0, "aplication/json");
           // request.ContentType.Insert(0, "aplication/json");
            //this.Request.ContentLength = 2000;
            this.Response = response;
           
            this.Body = body;

            int code = (int)response.StatusCode;
            if (code < 200 || code > 206)
            {
                this.Error = body.FromJSON<ResponseError>();
            }
        }

        public void Dispose()
        {
            this.Request = null;
            this.Response = null;
            this.Body = null;
        }
    }
}